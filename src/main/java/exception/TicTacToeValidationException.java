package main.java.exception;

/**
 * TicTacToe Validation Exception for managing all internal validations
 */
public class TicTacToeValidationException extends Exception {
	
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -5831994528438804942L;
	
	private int code; 
	
	/**
	 * TicTacToeValidationException constructor
	 * @param message
	 */
	public TicTacToeValidationException(TicTacToeValidationMessage message){
		super(message.getMessage());
		this.code = message.getCode();
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
}
