package main.java.exception;

/**
 * Class for keeping track of the validation exception.
 * FIXME could be extracted to user code/message instead of this
 */
public enum TicTacToeValidationMessage {
	CREATION(101, "The rows and cols defined for the TableBoard must be greater than the connection lenght defined for the match."),
	CELL_ERROR_RETRIEVING(102, "Error retrieving an empty cell"),
	CELL_INCORRECT_VALUE(103, "Incorrect value for the cell");
	
	private final String message;
	private final int code;
	
	/**
	 * TicTacToeValidationMessage construction
	 * @param message
	 */
	private TicTacToeValidationMessage(int code, String message){
		this.message = message;
		this.code = code;
	}
	
	/**
	 * Gets the message
	 * @return message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Returns the code
	 * @return code
	 */
	public int getCode(){
		return code;
	}
}
