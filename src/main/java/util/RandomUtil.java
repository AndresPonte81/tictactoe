package main.java.util;

/**
 * Util class in order to get a Random number
 */
public class RandomUtil {

	/**
	 * Gets a Random number
	 * @param maxValue max value expected
	 * @return 0|maxVakue random number
	 */
	public static int getRandomNumber(int maxValue){
		return (int) (Math.random() * maxValue);
	}
}
