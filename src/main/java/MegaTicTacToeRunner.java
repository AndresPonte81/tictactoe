package main.java;

import main.java.board.Cell;
import main.java.board.TableBoard;
import main.java.exception.TicTacToeValidationException;
import main.java.exception.TicTacToeValidationMessage;

/**
 * MegaTicTacToeRunner class
 * Responsible for the creation of the board and iteration logic
 */
public class MegaTicTacToeRunner {

	// TableBoard
	private TableBoard tableBoard;
	
	/**
	 * Main constructor for the TableBoard
	 */
	public MegaTicTacToeRunner() {
		
		// Creates the TableBoard
		tableBoard = new TableBoard();
		tableBoard.printConfig();
	}
	
	/**
	 * Main constructor for the TableBoard
	 * 
	 * @param rowsToSet number of rows to set
	 * @param colsToSet number of cols to set
	 * @throws TicTacToeValidationException thrown at creation of the TableBoard if the conditions are not reached
	 */
	public MegaTicTacToeRunner(int rowsToSet, int colsToSet) throws TicTacToeValidationException {
		
		// Creates the TableBoard
		tableBoard = new TableBoard(rowsToSet, colsToSet);
		tableBoard.printConfig();
	}
	
	/**
	 * Main constructor for the TableBoard
	 * 
	 * @param rowsToSet number of rows to set
	 * @param colsToSet number of cols to set
	 * @param connectionLength amount of continuous values for a win
	 * @throws TicTacToeValidationException thrown at creation of the TableBoard if the conditions are not reached
	 */
	public MegaTicTacToeRunner(int rowsToSet, int colsToSet, int connectionLength) throws TicTacToeValidationException {
		
		// Creates the TableBoard
		tableBoard = new TableBoard(rowsToSet, colsToSet, connectionLength);
		tableBoard.printConfig();
	}
	
	/**
	 * Runs the game
	 * @throws TicTacToeValidationException
	 */
	public void run() throws TicTacToeValidationException {
		
		// Boolean for control break
		boolean gameOver = false;
		
		// Iteration counter for Tie control
		int iterationCounter = 0;
		
		// While the game is not over
		do{
			// Retrieve an empty cell
			Cell c = tableBoard.getRandomEmptyCell();
			if(c != null){
				// Set the Value corresponding to the user
				c.setValue(getUserValue(iterationCounter++));
				
				// Print the TableBoard
				tableBoard.printBoard();
				
				// Verify Game Over
				gameOver = tableBoard.isGameOver(iterationCounter);
			}else{
				throw new TicTacToeValidationException(TicTacToeValidationMessage.CELL_ERROR_RETRIEVING);
			}
		}while(!gameOver);
	}
	
	/**
	 * Gets the User Value, depending on the iteration
	 * @param i iteration counter
	 * @return user value (0 or 1)
	 */
	private static int getUserValue(int i){
		return ((i % 2) == 0) ? 1 : 0;
	}
}
