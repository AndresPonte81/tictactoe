package main.java.board;

import main.java.exception.TicTacToeValidationMessage;
import main.java.exception.TicTacToeValidationException;
import main.java.util.RandomUtil;

/**
 * TableBoard, class responsible for the matrix creation and internal logic
 */
public class TableBoard {
	
	// Default value for row and cell definition
	private static final int DEFAULT_COUNT = 3;
	
	// Row and Col counter
	private final int rows;
	private final int cols;
	
	/*
	 * Amount of consecutive values for a Win
	 * Important: this value is also used as a restriction for the construction of the board, 
	 * as rows and cols must be > than this   
	 */
	private final int connectionLength;
	
	// Matrix of Cells, to be initialized on constructor with the rows, cols defined
	public Cell[][] tableBoardCells;

	/**
	 * TableBoard default Constructor
	 */
	public TableBoard() {
		super();
		this.rows = DEFAULT_COUNT;
		this.cols = DEFAULT_COUNT;
		this.connectionLength = DEFAULT_COUNT;
		initCells();
	}
	
	
	/**
	 * TableBoard Dynamic Constructor
	 * 
	 * @param rowsToSet amount of rows for the TableBoard, must be > connectionLength
	 * @param colsToSet amount of cols for the TableBoard, must be > connectionLength
	 */
	public TableBoard(int rowsToSet, int colsToSet) throws TicTacToeValidationException {
		super();
		this.rows = rowsToSet;
		this.cols = colsToSet;
		this.connectionLength = DEFAULT_COUNT;
		initializeTableBoard();
	}
	
	/**
	 * TableBoard Dynamic Constructor
	 * 
	 * @param rowsToSet amount of rows for the TableBoard, must be > connectionLength
	 * @param colsToSet amount of cols for the TableBoard, must be > connectionLength
	 * @param connectionLengthToSet amount of cells to win
	 */
	public TableBoard(int rowsToSet, int colsToSet, int connectionLengthToSet) throws TicTacToeValidationException {
		super();
		this.rows = rowsToSet;
		this.cols = colsToSet;
		this.connectionLength = connectionLengthToSet;
		initializeTableBoard();
	}
	
	/**
	 * Private method in order to iterate and initialize the matrix
	 * @throws TicTacToeValidationException when the connectionLength is greater than the cols or rows defined
	 */
	private void initializeTableBoard() throws TicTacToeValidationException {
		if(this.rows < this.connectionLength || this.cols < this.connectionLength){
			throw new TicTacToeValidationException(TicTacToeValidationMessage.CREATION);
		}
		initCells();
	}
	
	/**
	 * Private method in order to initialize the TableBoard cells, extracted in order to be used for empty constructor
	 */
	private void initCells(){
		// Initialize the Cells matrix with the defined row and col
		tableBoardCells = new Cell[rows][cols];
		
		// Iterate over the amount of rows defined
		for (int i = 0; i < this.rows; i++) {
			// Iterate over the amount of cols defined
			for (int j = 0; j < this.cols; j++) {
				tableBoardCells[i][j] = new Cell(i, j);
			}
		}
	}
	
	/**
	 * Prints the Table Board
	 */
	public void printBoard(){
		System.out.println("----------------");
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				tableBoardCells[i][j].printValue();
				System.out.print(" | ");
			}
			System.out.println("");
		}
	}
	
	/**
	 * Verifies if the Game is Over, for this it checks:
	 * 1- Iteration over each Row
	 * 2- Iteration over each Row
	 * 
	 * FIXME: should add iteration in order to check diagonal
	 * 
	 * @param iterationCounter counter of the game iteration in order to confirm a TIE
	 * @return true|false if the game is over or if it continues
	 */
	public boolean isGameOver(int iterationCounter){

		// Rows iteration
		for (int i = 0; i < this.rows; i++) {
			if(checkRow(this.tableBoardCells[i])){
				return true;
			}
		}
		
		// Columns iteration
		for (int j = 0; j < this.cols; j++) {
			if(checkColumn(j)){
				return true;
			}
		}
		
		// If the iteration counter is = max cell count
		if(iterationCounter == this.getCellsCount()){
			System.out.println("GAMEOVER? IT'S A TIE!");
			return true;
		}
		
		return false;
	}
	
	/**
	 * Checks if a winner was reached for a specific Row (Cell[])
	 * @param row the Row {@link Cell}
	 * @return true|false if a winner was reached
	 */
	private boolean checkRow(Cell[] row){
		/**
		 * Iterate over the cols for this row, and accumulate the equal continuous values
		 * Starting at 1 for the initial value, as the check starts from col 2
		 */
		int accumulatedEqualsValues = 1;
		// Starting at position 1 to compare against the previous
		for (int i = 1; i < this.cols; i++){
			// If the values differs (or the cell is empty), reset to 0
			if (!row[i].equals(row[i-1])){
				accumulatedEqualsValues = 0;
			}else{
				accumulatedEqualsValues++;
			}
			// If the accumulated values are equal to the connection length, a winner has been reached
			if(accumulatedEqualsValues == this.connectionLength){
				System.out.println("GAMEOVER? TRUE, WINNER: "+row[i].getValue() + " starting horizontally in row: ["+(row[i].getRow()+1)+ ", "+(row[i].getCol()-this.connectionLength+1)+"]" );
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if a winner was reached for a specific column (int column position)
	 * @param column column position
	 * @return true|false if a winner was reached
	 */
	private boolean checkColumn(int column){
		/**
		 * Iterate over the rows for this column, and accumulate the equal continuous values
		 * Starting at 1 for the initial value, as the check starts from row 2
		 */
		int accumulatedEqualsValues = 1;
		// Starting at position 1 to compare against the previous
		for (int i = 1; i < this.rows; i++){
			// If the values differs (or the cell is empty), reset to 0
			if (!this.tableBoardCells[i][column].equals(this.tableBoardCells[i-1][column])){
				accumulatedEqualsValues = 0;
			}else{
				accumulatedEqualsValues++;
			}
			// If the accumulated values are equal to the connection length, a winner has been reached
			if(accumulatedEqualsValues == this.connectionLength){
				System.out.println("GAMEOVER? TRUE, WINNER: "+this.tableBoardCells[i][column].getValue() + " starting vertically in column: ["+(this.tableBoardCells[i][column].getRow()-this.connectionLength+1)+ ", "+(column+1)+"]" );
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets a random empty Cell {@link Cell}
	 * @return Cell or null if non found
	 */
	public Cell getRandomEmptyCell(){
		Cell emptyCell = null;
		
		while(emptyCell == null){
			// Get Random position
			int rowPosition = RandomUtil.getRandomNumber(this.rows);
			int colPosition = RandomUtil.getRandomNumber(this.cols);
			
			// Retrieve the corresponding Cell
			Cell c = this.tableBoardCells[rowPosition][colPosition];
			
			// Verify empty cell
			if(c.isEmpty()){
				emptyCell = c;
			}
		}
		return emptyCell ;
	}
	
	/**
	 * Gets the amount of Cell for the Table Board 
	 * @return count of cells
	 */
	public int getCellsCount(){
		return this.rows * this.cols;
	}
	
	/**
	 * Return the rows defined for the TableBoard
	 * @return number of rows
	 */
	public int getRows() {
		return rows;
	}


	/**
	 * Return the cols defined for the TableBoard
	 * @return number of cols
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * Returns the connection length.
	 * This is the amount of consecutive cells with the same value needed for a Win
	 * @return connection length
	 */
	public int getConnectionLength(){
		return this.connectionLength;
	}
	
	/**
	 * Prints on sysou the TableBoard configuration
	 */
	public void printConfig(){
		System.out.println("TableBoard: " + this.rows + " by "+ this.cols + " with a connection lenght of: "+this.connectionLength);
	}

}
