package main.java.board;

import main.java.exception.TicTacToeValidationException;
import main.java.exception.TicTacToeValidationMessage;

/**
 * Cell class containing a value for a [row, col] position
 */
public class Cell {
	
	private int value;
	private int row;
	private int col;
	
	private static final int EMPTY_VALUE = -1;
	
	/**
	 * Empty constructor
	 * @param row int row number
	 * @param col int col number
	 */
	public Cell(int row, int col) {
		super();
		this.value = -1;
		this.row = row;
		this.col = col;
	}
	
	/**
	 * Full constructor with value
	 * @param value
	 * @param row
	 * @param col
	 * @throws TicTacToeValidationException when the value assigned is not within the possible ones
	 */
	public Cell(int value, int row, int col) throws TicTacToeValidationException {
		super();
		// Extract to enum?
		if(value != 0 || value != 1){
			throw new TicTacToeValidationException(TicTacToeValidationMessage.CELL_INCORRECT_VALUE);
		}
		this.value = value;
		this.row = row;
		this.col = col;
	}
	
	/**
	 * Prints the value of the cell.
	 * Prints - when empty
	 */
	public void printValue(){
		if(this.value == -1){
			System.out.print("-");
		}else{
			System.out.print(this.value);
		}
	}
	
	/**
	 * Compares value against empty one (-1)
	 * @return
	 */
	public boolean isEmpty(){
		return this.value == EMPTY_VALUE;
	}
	
	/**
	 * Gets the value
	 * @return value for the Cell
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the value
	 * @param value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}
	
	/**
	 * Gets the row
	 * @return row of the Cell
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Sets the row
	 * @param row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * Gets the col
	 * @return col of the Cell
	 */
	public int getCol() {
		return col;
	}

	/**
	 * Sets the col
	 * @param col
	 */
	public void setCol(int col) {
		this.col = col;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	/**
	 * Overwritten in order to compare also for non empty values
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (value != other.value)
			return false;
		// Add not empty to equals comparison
		if (this.isEmpty() || other.isEmpty()){
			return false;
		}
		return true;
	}

}
