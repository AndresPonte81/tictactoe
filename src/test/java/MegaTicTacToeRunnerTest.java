package test.java;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import main.java.MegaTicTacToeRunner;
import main.java.exception.TicTacToeValidationMessage;
import main.java.exception.TicTacToeValidationException;

/**
 * MegaTicTacToeRunner Test
 */
public class MegaTicTacToeRunnerTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDefault() {
		try {
			// Example with a 3-3 board with default connection length
			MegaTicTacToeRunner runner = new MegaTicTacToeRunner();
			runner.run();
		} catch (TicTacToeValidationException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testExtended() {
		try {
			// Example with a 20-19 board with connection length of 14
			MegaTicTacToeRunner runner = new MegaTicTacToeRunner(20, 19, 5);
			runner.run();
		} catch (TicTacToeValidationException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testValidatioExpection() {
		try {
			// Example with a 12-12 board with 13 connection length
			MegaTicTacToeRunner runner = new MegaTicTacToeRunner(12, 12, 13);
			runner.run();
			fail("Creation failed");
		} catch (TicTacToeValidationException e) {
			assertEquals(e.getCode(), TicTacToeValidationMessage.CREATION.getCode());
		}
	}
	
	

}
