# MegaTicTacToe README

## Instructions
We would like to give you a small take-home problem. You can continue building on top of the TicTacToe problem you did with Utkarsh. The extended problem is: 
You run a game app and the first game you shipped was TicTacToe. Now your customers are demanding "MegaTicTacToe" within the same app. MegaTicTacToe is exactly like TicTacToe but rather than a 3x3 board, you have a 8x8 board but connection length is still 3. Acceptable "connections" are still vertical and horizontal (let's ignore diagonal). Also, your customers can demand more crazy TicTacToe like games too, so make sure your code is maintainable.

## Comments 

### I created a test in order to test some of the possible outcomes of the program. In case a 8x8 game is intended with a 3 connection length the call should be similar to:
```MegaTicTacToeRunner runner = new MegaTicTacToeRunner(8, 8);
```runner.run();

### For a 12x11 game with a 5 connection length the call should be similar to:
```MegaTicTacToeRunner runner = new MegaTicTacToeRunner(12, 11, 5);
```runner.run();